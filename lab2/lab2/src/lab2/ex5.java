package lab2;
import java.util.*;
public class ex5 {	
	    public static void main(String[] args){	      
	        Random r = new Random();	 
	        int[] a = new int[10];	 
	        for(int i=0;i<a.length;i++){
	            a[i] = r.nextInt(100);
	        }	
	        System.out.print("Initial vector is: ");
	        for(int i=0;i<a.length;i++){
	            System.out.print("a["+i+"]="+a[i]+" ");
	        }	
	        for (int i = 0; i < 10; i++) 
	            for (int j = 0; j < 10-i-1; j++) 
	                if (a[j] > a[j+1]) 
	                { 	                    
	                    int aux = a[j]; 
	                    a[j] = a[j+1]; 
	                    a[j+1] = aux; 
	                }         
	        System.out.print("\nSorted vector is: ");
	        for(int i=0;i<a.length;i++){
	            System.out.print("a["+i+"]="+a[i]+" ");
	        }	 
	 
	    }    
}
