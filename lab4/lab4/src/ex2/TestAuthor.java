package ex2;

public class TestAuthor {
	public static void main(String[] args)
	{
		Author a1 = new Author("Mihaela Popescu", "miha.popescu@gmail.com", 'f');
		System.out.println("name: " + a1.getName());
		a1.getEmail();
		System.out.println("gender:" + a1.getGender());
		System.out.println( a1.toString());
		a1.setEmail("miha@gmail.com");
		System.out.println( a1.toString());
	}
}
