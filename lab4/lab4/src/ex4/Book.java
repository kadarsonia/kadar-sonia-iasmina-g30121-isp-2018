package ex4;
import ex3.Author;
public class Book {
	private String name;
	private Author[] authors;
	private double price;
	private int qtyInStock=0;
	public Book(String name, Author[] authors, double price)
	{
		this.name = name;
		this.authors = authors;
		this.price =price;
	}
	public Book(String name, Author[] authors, double price, int qtyInStock)
	{
		this.name = name;
		this.authors = authors;
		this.price =price;
		this.qtyInStock= qtyInStock;
	}
	public String getName()
	{
		return name;
	}
	public Author[] getAuthors()
	{
		return authors;
	}
	public double getPrice()
	{
		return price;
	}
	public void setPrice(double price)
	{
		this.price=price;
	}
	public int getQtyInStock()
	{
		return qtyInStock; 
	}
	public void setQtyInStock()
	{
		this.qtyInStock=qtyInStock;
	}
	
	public void  printAuthors()
	{
		for(Author author: authors )
		{
			System.out.print(author.getName()+ " ");
		}
	}
	
	public int nrAuthors()
	{
		return this.authors.length;
	}
	public String toString()
	{
		return this.name + " by " + this.nrAuthors() + " authors: " ;
	}
}
