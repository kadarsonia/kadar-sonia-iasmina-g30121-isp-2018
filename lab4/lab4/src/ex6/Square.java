package ex6;

public class Square extends Rectangle 
{
	private double side=1.0;
	public Square()
	{
		this.side=1.0;
	}
	public Square(double s)
	{
		this.side=s;
	}
	public Square(double s, String c, boolean f)
	{
		super(s,s,c,f);
		this.side=s;
	}
	public double getSide()
	{
		return side;
	}
	public void setWidth(double s)
	{
		this.side=s;
	}
	public void setLenght(double s)
	{
		this.side=s;
	}
	public String toString()
	{
		return "A Square with side" + side + "which is subclass of " + super.toString(); 
	}
}
