package ex6;

public class Rectangle extends Shape
{
	private double width=1.0;
	private double lenght=1.0;
	public Rectangle()
	{
		this.width=1.0;
		this.lenght=1.0;
	}
	public Rectangle(double w, double l)
	{
		this.width=w;
		this.lenght=l;
	}
	public Rectangle(double w , double l, String c, boolean f)
	{
		super(c,f);
		this.width=w;
		this.lenght=l;
	}
	public double getWidth()
	{
		return width;
	}
	public void setWidth(double w)
	{
		this.width=w;
	}
	public double getLenght()
	{
		return lenght;
	}
	public void setLenght(double l)
	{
		this.lenght=l;
	}
	public double getArea()
	{
		return lenght*width;
	}
	public double getPerimeter()
	{
		return (lenght+width)*2;
	}
	public String toString()
	{
		return "A rectangle with " + width + " and " + lenght + " which is a subclass of "+ super.toString();
	}
}
