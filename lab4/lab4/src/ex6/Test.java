package ex6;

public class Test 
{
	public static void main(String[] args)
	{
		Shape s1= new Shape("yellow",false);
		Circle c1= new Circle(2.2, "green",true);
		Rectangle r1= new Rectangle (2.0, 4.0,"red",false);
		Square s2= new Square(5.0, "black",true);
		System.out.println(s1.toString());
		System.out.println(c1.toString());
		System.out.println(r1.toString());
		System.out.println(s2.toString());
		System.out.println("Area of circle is " + c1.getArea() + " and perimeter of circle is " + c1.getPerimeter());
		System.out.println("Area of rectangle is " + r1.getArea() + " and perimeeter of rectangler is " + r1.getPerimeter());
		System.out.println("Area of square is " + s2.getArea() + " and perimeter of square is " + s2.getPerimeter());
		
	}
}
