package ex6;
public class Shape 
{
	private String color = "red";
	private boolean filled = true;
	public Shape()
	{
		this.color = "green";
		this.filled = true;
	}
	public Shape(String c, boolean f)
	{
		this.color = c;
		this.filled = f;		
	}
	public String getColor()
	{
		return color;
	}
	public void setColor(String c)
	{
		this.color= c;
	}
	public boolean isFilled()
	{	
		return filled;
	}
	public void setFilled(boolean f)
	{
		this.filled= f;
	}
	public String toString()
	{   String isfilled;
		if(isFilled()==true)
			isfilled = "filled";
		  else
			isfilled = "not filled";
		return "A Shape with color of "+ getColor() + " and is " + isfilled;
	}
}
