package ex6;

public class Circle extends Shape {

			double radius=1.0;
			
			public Circle()
			{				 	
			 	this.radius=1.0;
			}
			public Circle(double r)
			
			{
				this.radius=r;
			}
			public Circle(double r, String c, boolean filled)
			{	
				super(c,filled);
				this.radius=r;
			}
			public double getRadius()
			{
				return radius;
			}	
			public void setRadius(double r)
			{
				this.radius=r;
			}
			public double getArea()
			{
				 return  radius*radius*Math.PI;
			}
			public double getPerimeter()
			{
				 return  2*Math.PI*radius;
			}
			public String toString() 
			{
				return "A Circle with radius=" + radius + ", which is a subclass of" + super.toString();
			}
			
			 
			
}
