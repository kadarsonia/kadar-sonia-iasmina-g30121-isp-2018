package ex1;

public class Circle {

		double radius;
		String color= "red";
		
		public Circle() {				 	
		 	this.radius=1.0;
	        this.color = "red";
		}
		
		public Circle(double r){
			this.radius=r;
		}
		
		 public double getRadius(){
			 return radius;
		}

		 public double getArea(){
			 return  radius*radius*Math.PI;
		}

		public String toString() {
			return "Circle with radius=" + radius + ".";
		}
		
		
}
