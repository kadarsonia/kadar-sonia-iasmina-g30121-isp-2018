package ex5;
import ex1.Circle;
public class Cylinder extends Circle
{
	private double height=1.0;
	public Cylinder()
	{
		super();
	}
	public Cylinder(double r)
	{
		super(r);		
	}
	public Cylinder(double r, double h)
	{
		super(r);
		this.height = h;		
	}
	public double getHeight()
	{
		return height;
	}
	
	public double getVolume()
	{
		return super.getArea()*height;
	}
}