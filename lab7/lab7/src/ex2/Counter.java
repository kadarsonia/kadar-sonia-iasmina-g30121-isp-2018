package ex2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
public class Counter {	
	    private Path file;
	    private char c;

	    Counter(Path file, char c){
	        this.file = file;
	        this.c = c;
	    }

	    public int count(){
	        int count = 0;
	        try{
	        	BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(file)));
	            String line = null;
	            line = br.readLine();
	            while(line != null){
	                for(int i = 0; i < line.length(); i++){
	                    if(line.charAt(i) == c){
	                        count++;
	                    }
	                }
	            }
	        } catch (IOException x){
	            System.err.println(x);
	        }
	        return count;
	    }
	}


