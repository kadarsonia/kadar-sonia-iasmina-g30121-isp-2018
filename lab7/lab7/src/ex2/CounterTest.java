package ex2;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class CounterTest {

	public static void main(String[] args) {
	    Path file = Paths.get("D:/Sonia/Facultate/sem2/ISP/laboratoare/lab7/lab7/src/ex2/data.txt");
        System.out.println("What character you want to search for?");
        Scanner in = new Scanner(System.in);
        char c = in.next().charAt(0);
	    Counter counter = new Counter(file, c);
        System.out.println(counter.count());
	}

}
