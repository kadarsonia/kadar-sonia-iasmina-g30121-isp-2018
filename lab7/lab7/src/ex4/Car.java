package ex4;

import java.io.*;

public class Car implements  Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String model;
	double price;
	
	public Car(){
		this.model = "ModelTest";
		this.price = 1000;
	}

	public Car(String model , double price)
	{
		this.model = model;
		this.price = price;
	}
	
	
	public String getLast() {
        return model;
    }

     void save(String fileName) {
           try {
               ObjectOutputStream o =
                 new ObjectOutputStream(
                   new FileOutputStream(fileName));
               o.writeObject(this);   
               System.out.println("Car saved in file");
               o.close();
         } catch (IOException e) {
        	 System.err.println("Car can not be saved in file.");
               e.printStackTrace();
         }

     }

     static Car load(String fileName) throws IOException, ClassNotFoundException { 
    	 ObjectInputStream	in = new ObjectInputStream(new FileInputStream(fileName));
                Car c = (Car)in.readObject();
                System.out.print(c.toString());
                in.close();
                return c;         
      }

     public String toString1(){
			return "Car = "+model+" price "+price;
			}
      	
}
	
