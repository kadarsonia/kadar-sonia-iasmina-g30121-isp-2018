package ex4;

import java.io.Serializable;

public class CarCreator implements  Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Car createCar(String model, double price)
	{ 	
		Car c = new Car(model, price);
		System.out.println(c+" new car");
		return c;
	}
}
