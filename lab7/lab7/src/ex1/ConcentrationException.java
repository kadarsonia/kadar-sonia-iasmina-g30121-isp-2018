package ex1;

public class ConcentrationException extends Exception {
	 int c;
	 String msg;
     public ConcentrationException(int c,String msg) {
           super();
           this.c = c;
           this.msg = msg;
     }

     int getConc(){
           return c;
     }

     public String getMessage(){
     	return msg;
     }
	}

