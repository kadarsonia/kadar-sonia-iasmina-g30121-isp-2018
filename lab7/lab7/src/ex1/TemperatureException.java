package ex1;

public class TemperatureException  extends Exception {
	int t;
	String msg;
    public TemperatureException(int t,String msg) {
          super();
          this.t = t;
          this.msg = msg;
    }

    int getTemp(){
          return t;
    }
    
    public String getMessage(){
    	return msg;
    }
}
