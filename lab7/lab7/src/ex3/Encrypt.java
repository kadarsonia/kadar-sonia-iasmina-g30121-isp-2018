package ex3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;

public class Encrypt {
	private Path file;

    
    Encrypt(Path file){
        this.file = file;
    }

    public void encrypt(){
    	String encryptedstring = "";
        try{
        	
        	BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(file)));
        	char c;
            String line = null;
            line = br.readLine();            
            while(line != null){
                for(int i = 0; i < line.length(); i++){
                    	c= (char)(line.charAt(i) << 1);
                    	encryptedstring +=c;
                    }
             line = br.readLine();    
            }
        } catch (IOException x){
            System.err.println(x);
       }
        writeFile(encryptedstring);
    }
  
    public void writeFile(String  encryptedstring){
	    try{
	    	PrintWriter writer = new PrintWriter("D:/Sonia/Facultate/sem2/ISP/laboratoare/lab7/lab7/src/ex3/text.enc", "UTF-8");
	    	writer.println(encryptedstring);
	    	writer.close();
	    	System.out.println(encryptedstring);
	    }
	    catch (IOException x){
	    	System.err.println(x);
	    }
    }
  }

