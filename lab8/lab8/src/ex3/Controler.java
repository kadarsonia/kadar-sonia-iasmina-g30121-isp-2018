package ex3;

import java.util.ArrayList;

public class Controler {
	String stationName; 
	Controler neighbourController;
	ArrayList<Controler> listneighbourController = new ArrayList<Controler>();
	ArrayList<Segment> list = new ArrayList<Segment>();
	public Controler(String gara){
		stationName = gara;
	}
	void setNeighbourControlers(ArrayList<Controler>  v){
		listneighbourController= v;
	}
	void setNeighbourController(Controler v){
		neighbourController = v;
	}
 
	void addNeighbourController(Controler v){
		listneighbourController.add(v);
	}
	
	void addControlledSegment(Segment s){
		list.add(s);
	}
	
	int getFreeSegmentId(){
		for(Segment s:list){
			if(s.hasTrain()==false)
				return s.id;
		}
		return -1;
	}
	void controlStep(){
		for(Segment segment:list){
			if(segment.hasTrain()){
				Train t = segment.getTrain();
				
				if(t.getDestination().equals(neighbourController.stationName)){
					int id = neighbourController.getFreeSegmentId();
					if(id==-1){
						System.out.println("Trenul "+t.name+"din gara" +stationName+" nu poate fi trimis catre "+neighbourController.stationName+". Nici un segment disponibil!");
							return;	
					}
					System.out.println("Trenul "+t.name+" pleaca din gara "+stationName +" spre gara "+neighbourController.stationName);
					segment.departTrain();
					neighbourController.arriveTrain(t,id);
				}
			}
		}
	}
	
	public void arriveTrain(Train t, int idSegment){
		for(Segment segment:list){
			if(segment.id == idSegment)
				if(segment.hasTrain()==true){
					System.out.println("Crash! Trai "+t.name+"colided with"+segment.getTrain().name+"on segment "+segment.id+" in station "+stationName);
					return;
				}
				else{ 
					System.out.println("Train "+t.name+" arrived on segment "+segment.id+" in station"+stationName);
					segment.arriveTrain(t);
					return;
				}
			}
		System.out.println("Train "+t.name+" cannot be recived "+stationName+". Check controller logic algorithm!");
	}
	public void displayStationState(){
		System.out.println("=== STATION "+stationName+" ===");
		for(Segment s:list){
			if(s.hasTrain())
				System.out.println("|----------ID="+s.id+"__Train=______ catre ________----------|");
		}
	}
}
