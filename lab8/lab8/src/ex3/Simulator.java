package ex3;

public class Simulator {
	public static void main(String[] args)
	{
		Controler c1= new Controler("Cluj-Napoca");
		Segment s1 = new Segment(1);
		Segment s2 = new Segment(2);
		Segment s3 = new Segment(3);
		c1.addControlledSegment(s1);
		c1.addControlledSegment(s2);
		c1.addControlledSegment(s3);
		
		Controler c2 = new Controler("Bucuresti");
		
		Segment s4 = new Segment(4);
		Segment s5 = new Segment(5);
		Segment s6 = new Segment(6);
		
		c2.addControlledSegment(s4);
		c2.addControlledSegment(s5);
		c2.addControlledSegment(s6);
	
		c1.setNeighbourController(c2);
		c2.setNeighbourController(c1);
		
		Controler c3 = new Controler("Timisoara");
		
		Segment s7 = new Segment(7);
		Segment s8 = new Segment(8);
		Segment s9 = new Segment(9);
		
		c2.addControlledSegment(s7);
		c2.addControlledSegment(s8);
		c2.addControlledSegment(s9);
	
		c3.addNeighbourController(c2);
		c3.addNeighbourController(c1);

		System.out.println("\nStart train control\n");
		
		
		
		
		for(int i=0;i<4;i++){
			System.out.println("### step"+i+"###");
			c1.controlStep();
			c2.controlStep();
			c3.controlStep();
			System.out.println();
			
			c1.displayStationState();
			c2.displayStationState();
			c3.displayStationState();
		}
	}

}
