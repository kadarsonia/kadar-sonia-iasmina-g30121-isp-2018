package ex4;

public class HeatingUnit extends Unit{
	private boolean heating;
	
	HeatingUnit(boolean heating){
		super(UnitType.ALARM.HEATING);
		this.heating= heating;
	}
	boolean isHeating(){
			return heating;
		}
		public String toString(){
			return "HeatingUnit{" + "heating="+heating+"}";
		}

}
