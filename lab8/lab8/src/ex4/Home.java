package ex4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public abstract class Home {
	private Path file;
	private Random r = new Random();
    private final int SIMULATION_STEPS = 20;
    private Unit u;
    public Home() {
		// TODO Auto-generated constructor stub
	}
	protected abstract void setValueInEnvironment(Event event);
    protected abstract void controllStep();
    protected abstract void setHomeUnit(Unit u);
    private Event getHomeEvent(){
        //randomly generate a new event;            
        int k = r.nextInt(100);
        if(k<30)
            return new NoEvent();
        else if(k<60){
            FireEvent f =  new FireEvent(r.nextBoolean());
            if(f.isSmoke() == true)
            	 {setHomeUnit(new AlarmUnit(true));
                 setHomeUnit(new GsmUnit(r.nextBoolean()));
            	 }
            return f;
        }
        else
            { TemperatureEvent t = new TemperatureEvent(r.nextInt(50));    
              if(t.getVlaue()<23) 
            	  setHomeUnit(new HeatingUnit(true));
              else  
            	  setHomeUnit(new CoolingUnit(true));
              return t;
            }         
    }   
    
    public void simulate(){
        int k = 0;
        while(k <SIMULATION_STEPS){
            Event event = this.getHomeEvent();
            setValueInEnvironment(event);  
            controllStep();           
            try {
                Thread.sleep(300);
            } catch (InterruptedException ex) {
               ex.printStackTrace();
            }
 
            k++;
        }
    }
    
    public static void writeFile(String message){
	    try{
	    	PrintWriter writer = new PrintWriter("D:/Sonia/Facultate/sem2/ISP/laboratoare/lab8/lab8/src/ex4/system_logs.txt", "UTF-8");
	    	writer.println(message);
	    	writer.close();
	    	System.out.println(message);
	    }
	    catch (IOException x){
	    	System.err.println(x);
	    }
}

}
