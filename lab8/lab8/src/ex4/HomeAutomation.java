package ex4;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;

public class HomeAutomation {
	private HomeAutomation(){}
	private static String messageFinal;
	public static void main(String[] args){
		
        //test using an annonimous inner class 
		
		
		Home h = new Home(){    			
            protected void setValueInEnvironment(Event event){
            	String message = "New event in environment "+event;
            	System.out.println(message); 
            	messageFinal += message;
            }
            protected void controllStep(){
            	String message = "Control step executed";
                System.out.println(message);
                messageFinal += message;
                writeFile(message);
            }
            protected void setHomeUnit(Unit u ){
            	messageFinal = "";
            	String message = "New unit in environment "+u;
            	 System.out.println(message);
            	messageFinal += message; 
            }
        
            };
        h.simulate();
        h.writeFile(messageFinal);
    }
	
	
}