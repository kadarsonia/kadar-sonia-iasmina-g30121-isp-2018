package ex4;

public class GsmUnit extends Unit{
	private boolean gsm;
	 
	GsmUnit(boolean gsm) {
        super(UnitType.GSM);
        this.gsm = gsm;
        if(gsm == true)
           callOwner();
    }
 
	boolean isGsm() {
        return gsm;
    }
 
    @Override
    public String toString() {
        return "GsmUnit{" + "gsm=" + gsm + '}';
    }      
    
    private void callOwner(){
    	System.out.println("Calling owner");
    }
         
 
}
