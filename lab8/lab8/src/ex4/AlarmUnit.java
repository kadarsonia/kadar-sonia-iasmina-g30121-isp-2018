package ex4;

public class AlarmUnit extends Unit {
	private boolean alarm;
	
	AlarmUnit(boolean alarm){
		super(UnitType.ALARM);
		this.alarm = alarm;
	}
	boolean startAlarm() {
	        return alarm;
	    }
	 
	    @Override
	    public String toString() {
	        return "AlarmUnit{" + "alarm=" + alarm + '}';
	    }

}
