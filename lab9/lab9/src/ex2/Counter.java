package ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Counter extends JFrame{

	JLabel count;
    JTextField tCount;
    JButton bCount;
    int ct=0;
    Counter()
    {
        setTitle("Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(250,250);
        setVisible(true);
    }

    public void init()
    {
        this.setLayout(null);
        int width=180; int height=20;

        count=new JLabel("Counter");
        count.setBounds(10,50,width,height);

        tCount=new JTextField(Integer.toString(ct));
        tCount.setBounds(10,100,width,height);

        bCount=new JButton("COUNT");
        bCount.setBounds(10,150,width, height);
        bCount.addActionListener(new TratareButonCounter());
        add(count);add(tCount);add(bCount);
    }

    public static void main(String[] args) {
        new Counter();

    }

    class TratareButonCounter implements ActionListener{
        public void actionPerformed(ActionEvent e)
        {
            ct++;
            String str = Integer.toString(ct);
            tCount.setText(str);
        }
    }
}
