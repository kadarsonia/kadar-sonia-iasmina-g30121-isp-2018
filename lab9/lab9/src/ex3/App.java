package ex3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class App extends JFrame {
	 
      JLabel user;
      JTextField tUser;
      JButton bLoghin;
      JTextArea area;
 
      App(){
    	  	setTitle("Click here");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            init();
            setSize(300,300);
            setVisible(true);
      }
 
      public void init(){
 
            this.setLayout(null);
            int width=80;
            int height = 20;
 
            user = new JLabel("Content file ");
            user.setBounds(10, 50, width, height);
            tUser = new JTextField();
            tUser.setBounds(70,50,width, height);
            bLoghin = new JButton("Display");
            bLoghin.setBounds(10,150,width, height);
 
            add(user);
            add(tUser);
            add(bLoghin);
            add(area);
 
      }
 
      public static void main(String[] args) {
            new App();
      }
      class e implements ActionListener {

          @Override
          public void actionPerformed(ActionEvent e) {
              area.setText("");
              try {
                  Scanner in=new Scanner(new File(tUser.getText()));
                  while(in.hasNextLine()){
                      area.append(in.nextLine());
                  }

              } catch (FileNotFoundException e1) {
                  e1.printStackTrace();
              }
          }
      }
}
