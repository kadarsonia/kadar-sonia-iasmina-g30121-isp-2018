package ex3;

public class Counter2 extends Thread {
	int poz;
	Counter2(String name, int poz){
        super(name);
        this.poz=poz;
	}
	
	public void run(){
        for(int i=poz;i<poz+100;i++){
              System.out.println(getName() + " i = "+i);
              try {
                    Thread.sleep((int)(Math.random() * 1000));
              } catch (InterruptedException e) {
                    e.printStackTrace();
              }
        }
  }
  	public static void main(String[] args) {
      Counter2 c1 = new Counter2("counter1",0);
      Counter2 c2 = new Counter2("counter2",100);
      c1.run();
      c2.run();
  }
}
