package ex4;
import ex3.LightSensor;
import ex3.TemperatureSensor;
public class Controller {
	private Controller(){
		}
	public static void control()
	{
		LightSensor sensor1 = new LightSensor();
		TemperatureSensor sensor2 = new TemperatureSensor();
		for(int i=0; i<20; i++)
		{
			System.out.println("At second " + i + " light value is " + sensor1.readValue() + " and temperature value is " + sensor2.readValue());
			try
			{
				 Thread.sleep(1000);
			}
			catch(InterruptedException ex)
			{
			    Thread.currentThread().interrupt();
			}
		}
	}

}
