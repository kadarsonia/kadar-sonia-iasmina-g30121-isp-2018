package ex1;

public class Square extends Rectangle
{
	
	
	public Square()
	{
		super();
	}
	public Square(double side)
	{
		super(side,side);
	}
	public Square(double side, String color, boolean filled)
	{
		super(side,side,color,filled);
		
	}
	public double getSide()
	{
		return super.getLenght();
	}
	public void setSide(double side)
	{
		super.setWidth(side);
		super.setLenght(side);
	}
	public void setWidth(double side)
	{
		super.setWidth(side);
	}
	public void setLenght(double side)
	{
		super.setLenght(side);
	}
	public String toString()
	{
		return " A Square with side " + getSide() + " which is subclass of " + super.toString(); 
	}
}
