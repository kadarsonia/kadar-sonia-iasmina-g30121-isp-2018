package ex1;

public class Rectangle extends Shape {
	protected double width=1.0;
	protected double lenght=1.0;
	public Rectangle()
	{
		this.width=1.0;
		this.lenght=1.0;
	}
	public Rectangle(double width, double lenght)
	{
		this.width=width;
		this.lenght=lenght;
	}
	public Rectangle(double width , double lenght, String color, boolean filled)
	{
		super(color,filled);
		this.width=width;
		this.lenght=lenght;
	}
	public double getWidth()
	{
		return width;
	}
	public void setWidth(double width)
	{
		this.width=width;
	}
	public double getLenght()
	{
		return lenght;
	}
	public void setLenght(double lenght)
	{
		this.lenght=lenght;
	}
	public double getArea()
	{
		return lenght*width;
	}
	public double getPerimeter()
	{
		return (lenght+width)*2;
	}
	public String toString()
	{
		return " A rectangle with width " + width + " and lenght " + lenght + " which is a subclass of "+ super.toString();
	}

}
