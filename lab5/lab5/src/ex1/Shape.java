package ex1;

abstract class Shape 
{
	protected String color;
	protected boolean filled;
	public Shape()
	{
		this.color = "green";
		this.filled = true;
	}
	public Shape(String color, boolean filled)
	{
		this.color = color;
		this.filled = filled;		
	}
	public String getColor()
	{
		return color;
	}
	public void setColor(String color)
	{
		this.color= color;
	}
	public boolean isFilled()
	{	
		return filled;
	}
	public void setFilled(boolean f)
	{
		this.filled= f;
	}
	abstract public double getArea();
	abstract public double getPerimeter();

	public String toString()
	{   String isfilled;
		if(isFilled()==true)
			isfilled = "filled";
		  else
			isfilled = "not filled";
		return " A Shape with color of "+ getColor() + " and is " + isfilled;
	}

}
