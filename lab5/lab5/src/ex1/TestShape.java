package ex1;

public class TestShape
{
	public static void main(String[] args)
	{	
		Shape s1 = new Circle();
		Shape s2 = new Circle(2.0);
		Shape s3 = new Circle(3.0, "red", true);
		Shape s4 = new Rectangle();
		Shape s5 = new Rectangle(2.0, 3.0);
		Shape s6 = new Rectangle(2.0, 3.0, "yelow", false);
		Shape s7 = new Square();
		Shape s8 = new Square(2.0);
		Shape s9 = new Square(2.0, "blue", true);
		Shape[] shapes = {s1,s2,s3,s4,s5,s6,s7,s8,s9};		
		for(Shape shape: shapes )
			{
				System.out.println(shape.toString() + " has area equal " + shape.getArea() + " and perimeter equal " + shape.getPerimeter());
			}
		
		
	}
}