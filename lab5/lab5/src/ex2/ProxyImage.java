package ex2;

public class ProxyImage implements Image
{
	   private Image image;
	   private String fileName;
	   private String imageType;
	 
	   public ProxyImage(String fileName, String imageType){
	      this.fileName = fileName;
	      this.imageType = imageType;
	      if(imageType == "real")
	    	  this.image = new RealImage(fileName);
	       else if (imageType == "rotated")
	    	  this.image =  new RotatedImage(fileName);
	   }
	 
	   public void display() {
		      if(image == null){
		    	  if(imageType == "real")
			    	  this.image = new RealImage(fileName);
			      else if (imageType == "rotated")
			    	  this.image =  new RotatedImage(fileName);
		      }
		      image.display();
		   }
}
