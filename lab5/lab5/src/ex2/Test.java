package ex2;

public class Test 
{
	public static void main(String[] args)
	{
	ProxyImage i1= new ProxyImage("RealImage","real");
	ProxyImage i2= new ProxyImage("RotatedImage","rotated");
	i1.display();
	i2.display();
	
	}
	
}
