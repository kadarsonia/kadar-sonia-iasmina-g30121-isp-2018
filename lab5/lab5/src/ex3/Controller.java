package ex3;
public class Controller  
{
	public void control()
	{
		LightSensor sensor1 = new LightSensor();
		TemperatureSensor sensor2 = new TemperatureSensor();
		for(int i=0; i<20; i++)
		{
			System.out.println("At second " + i + " light value is " + sensor1.readValue() + " and temperature value is " + sensor2.readValue());
			try
			{
				 Thread.sleep(1000);
			}
			catch(InterruptedException ex)
			{
			    Thread.currentThread().interrupt();
			}
		}
	}
}
