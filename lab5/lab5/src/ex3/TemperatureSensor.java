package ex3;
import java.util.*;
public class TemperatureSensor extends Sensor 
{
		public int readValue()
		{
			Random r = new Random();
			return r.nextInt(100);
		}
}
