package ex3;

import ex2.Bank;

public class TestBankAccounts {
	public static void main(String[] args) {
	Bank bank = new Bank();
    bank.addAccount("Alin Mihai", 100.0);
    bank.addAccount("Alin Andrei", 300.0);
    bank.addAccount("Denis Alexandru", 120.0);
    bank.addAccount("Cosmin Daniel",80.0);
    System.out.println("Print Accounts by balance");
    bank.printAccounts();
    System.out.println("Print Accounts between limits");
    bank.printAccounts(60.0,100.0);
    System.out.println("Get Account by owner name");
    bank.getAccount("Alin Mihai");
    System.out.println("Get All Account order by name");
     bank.getAllAccounts();
     }

}
