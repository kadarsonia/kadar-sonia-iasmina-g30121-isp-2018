package ex2;
import ex1.BankAccount;
import java.util.*;

public class Bank {
	ArrayList<BankAccount> bankAccounts = new ArrayList<BankAccount>();
	
	public void addAccount(String owner, double balance){
		 BankAccount account= new BankAccount(owner, balance);
		 bankAccounts.add(account);
	}
	
    public void printAccounts(){
    	int sorted;
    	do{
    		sorted = 1;
    		for( int i=0; i<bankAccounts.size()- 1; i++){
    			BankAccount o1 = (BankAccount) bankAccounts.get(i);
                BankAccount o2  = (BankAccount) bankAccounts.get(i + 1);
                BankAccount o3 = new BankAccount(" ", 0);
    		if (o1.balance > o2.balance) {
                o3.owner = o1.owner;
                o3.balance = o1.balance;
                o1.owner = o2.owner;
                o1.balance = o2.balance;
                o2.owner = o3.owner;
                o2.balance = o3.balance;
                sorted = 0;
            }
    		}
    	}while(sorted == 0);
    	for (int i = 0; i < bankAccounts.size(); i++) {
            BankAccount b = (BankAccount) bankAccounts.get(i);
            System.out.println(b.owner + " " + b.balance);
        }
    }
    
    public void  printAccounts(double minBalance, double maxBalance){
    	for (int i = 0; i < bankAccounts.size(); i++) {
            BankAccount b = (BankAccount) bankAccounts.get(i);
            if (b.balance >= minBalance && b.balance <= maxBalance)
            	System.out.println(b.owner + " " + b.balance);
    	}
    }
    
    public BankAccount getAccount(String owner)
    {
    	for (int i = 0; i < bankAccounts.size(); i++) {
            BankAccount b = (BankAccount) bankAccounts.get(i);
            if (b.owner.equals(owner))
                System.out.println(b.owner + " " + b.balance);
        }
        return null;
    }
    public BankAccount getAllAccounts(){
    	int sorted;
    	do{
    		sorted = 1;
    		for( int i=0; i<bankAccounts.size()- 1; i++){
    			BankAccount o1 = (BankAccount) bankAccounts.get(i);
                BankAccount o2  = (BankAccount) bankAccounts.get(i + 1);
                BankAccount o3 = new BankAccount(" ", 0);
    		if (o1.owner.compareTo(o2.owner)>0) {
                o3.owner = o1.owner;
                o3.balance = o1.balance;
                o1.owner = o2.owner;
                o1.balance = o2.balance;
                o2.owner = o3.owner;
                o2.balance = o3.balance;
                sorted = 0;
            }
    		}
    	}while(sorted == 0);
    	for (int i = 0; i < bankAccounts.size(); i++) {
            BankAccount b = (BankAccount) bankAccounts.get(i);
            System.out.println(b.owner + " " + b.balance);
        }
		return null;
    }
}
