package ex4;

import java.io.*;

public class TestDictionary {
	 public static void main(String[] args) throws IOException{
	            Dictionary dict = new Dictionary();
	            char answer;
	            String line, explain;
	            BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));
	 
	            do {
	               System.out.println("Menu");
	               System.out.println("a - add word ");
	               System.out.println("s - search word");
	               System.out.println("p - print Dictionary");
	               System.out.println("e - exit");
	 
	               line = fluxIn.readLine();
	               answer = line.charAt(0);
	 
	               switch(answer) {
	                  case 'a': case 'A':
	                     System.out.println("write word:");
	                     line = fluxIn.readLine();
	                     if( line.length()>1) {
	                        System.out.println("write definition:");
	                        explain = fluxIn.readLine();
	                        dict.addWord(new Word(line), new Definition(explain));
	                     }
	                  break;
	                  case 's': case 'S':
	                     System.out.println("Search word:");
	                     line = fluxIn.readLine();
	                     if( line.length()>1) {
	                    	 Word x = new Word(line);
	                    	 explain = String.valueOf(dict.getDefinition(x));
	                        if (explain == null)
	                           System.out.println("it does not exists");
	                        else
	                           System.out.println("Explanation:" + explain);
	                     }
	                  break;
	                  case 'p': case 'P':
	                     System.out.println("Print :");
	                     dict.printDictionary();
	                  break;
	 
	               }
	            } while(answer!='e' && answer!='E');
	            System.out.println("Program finished");
	         }
}
