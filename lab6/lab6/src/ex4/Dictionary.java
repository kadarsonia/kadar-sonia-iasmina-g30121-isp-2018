package ex4;

import java.util.*;
import java.io.*;
import static java.lang.System.*;

public class Dictionary {
	HashMap dictionary = new HashMap();
	
	public void addWord(Word w, Definition d){
		if(dictionary.containsKey(w))
                System.out.println("Modify existing word!");            
          else              
               System.out.println("Add new word.");              
		dictionary.put(w, d);        
     }
	public Object getDefinition(Word w){
		 System.out.println("search " + w);
		 System.out.println(Object dictionary.containsKey(w));
		 return dictionary.get(w);
	}
	public void getAllWords(){
		for (Object word : dictionary.keySet()) System.out.println(word.toString());
	}
	public void getAllDefinitions() {

        for (Object definition : dictionary.values()) System.out.println(definition.toString());
    }
	public void printDictionary(){
		 System.out.println(dictionary);
	}
}
