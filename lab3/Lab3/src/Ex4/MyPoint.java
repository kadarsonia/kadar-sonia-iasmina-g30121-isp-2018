package Ex4;

public class MyPoint {
	    int x;
	    int y;
	public MyPoint() 
	{
        this.x = 0;
        this.y = 0;
    }
	public MyPoint(int a, int b) 
    {
        this.x = a;
        this.y = b;
    }
	public void setX(int x) 
    {
        this.x = x;
    }
	public void setY(int y) 
    {
        this.y = y;
    }
    public int getX() 
    {
        return x;
    }
    public int getY() 
    {
        return y;
    }
    public void setXY(int x, int y) 
    {
        this.x = x;
        this.y = y;
    }
    public String tString() 
    {
        return "(" + this.x + "," + this.y + ")";
    }
    public double distance(int x, int y) 
    {
        double d;
        d = Math.sqrt(Math.pow(x + this.x, 2) + Math.pow(y - this.y, 2));
        return d;
    }
    public double distance(MyPoint a)
    {
        double d;
        d = Math.sqrt(Math.pow(a.x - this.x, 2) + Math.pow(a.y - this.y, 2));
        return d;
    }
}
