package Ex3;

public class Author {
	String name;
	String email;
	char gender;
	public Author(String n, String e, char g)
	{	
	 	this.name= n;
	 	this.email= e;
	 	this.gender= g;
	}
	public String getName()
	{
		return name;
	}
	public char getGender()
	{
		return gender;
	}
	public void setEmail(String email)
	{
		this.email = email;
    }

	public String getEmail()
	{
		return email;
    }
	public void tString()
	{
		System.out.println(this.name + "(" + this.gender + ") at " + this.email);
	}
}
