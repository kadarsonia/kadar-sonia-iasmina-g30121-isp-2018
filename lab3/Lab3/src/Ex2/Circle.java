package Ex2;

public class Circle {
	double radius=1.0;;
	String color="red";
	
	public Circle() {	
		 	
		 	this.radius=1.0;
	        this.color = "red";
	    }
	public Circle(double r, String c ){
		this.radius=r;
        this.color=c;
		}
	
	 public double getRadius(){
		 return radius;
			 }

	 public double getArea(){
		 return  radius*radius*Math.PI;
			 }
	
}
